This document is intended for anyone to add any additional information that they think is important for Phase Two.
Please add any references as you find them.

References:

For AngularJS:
http://www.asp.net/web-api/overview/getting-started-with-aspnet-web-api/build-a-single-page-application-spa-with-aspnet-web-api-and-angularjs
http://docs.asp.net/en/latest/client-side/angular.html
http://weblogs.asp.net/dwahlin/angularjs-in-60-ish-minutes-the-ebook

For Xamarin.Forms / Xamarin:
https://developer.xamarin.com/guides/xamarin-forms/creating-mobile-apps-xamarin-forms/
https://developer.xamarin.com/guides/android/

For Single Page Applications:
http://www.asp.net/web-api/overview/getting-started-with-aspnet-web-api/build-a-single-page-application-spa-with-aspnet-web-api-and-angularjs

For RestSharp:
https://github.com/restsharp/RestSharp/wiki

For Web Api:
http://www.codeproject.com/Articles/424461/Implementing-Consuming-ASP-NET-WEB-API-from-JQuery
http://www.dotnetcurry.com/aspnet/1063/create-rest-service-using-aspnet-webapi
http://www.c-sharpcorner.com/UploadFile/2ed7ae/jsonresult-type-in-mvc/

For Front-End:
http://foundation.zurb.com/
http://www.layoutit.com/