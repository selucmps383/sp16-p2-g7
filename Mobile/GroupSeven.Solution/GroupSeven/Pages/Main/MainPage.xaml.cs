﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace GroupSeven
{
	public partial class MainPage : TabbedPage
	{
		public MainPage ()
		{
			
			InitializeComponent ();

			ToolbarItem logout = new ToolbarItem{
				Text = "Logout"
			};

			logout.Clicked += async (sender, e) => {
				Globals.apiKey = "";
				Globals.userId = -1;
				await Navigation.PushAsync(new LoginPage());
			};

			NavigationPage.SetHasBackButton (this, false);

			this.ToolbarItems.Add (logout);
			this.Children.Add (new ProductMainPage ());
			this.Children.Add ( new ManufacturerMainPage());
			this.Children.Add (new CategoryMainPage());
		}

		protected override bool OnBackButtonPressed()
		{
			return true;
		}
	}
}

