﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GroupSeven
{
	public partial class LoginPage : ContentPage
	{

		public LoginPage ()
		{
			InitializeComponent ();
			NavigationPage.SetHasBackButton(this,false);

			Padding = new Thickness(20, Device.OnPlatform(20, 20, 20),20,20);
			StyleButton btnLogin = new StyleButton ("Login");
			StyleLabel lblTitle = new StyleLabel ("John Candy \nInventory Manager\n", 40){
				HorizontalOptions = LayoutOptions.Center
			};
			Image imgMain = new Image{
				Source = ImageSource.FromUri(new Uri(ImageSources.imgJohn))
			};

			Entry txtUsername = new Entry{				
				Placeholder = "Username", 
				Keyboard = Keyboard.Text,
			};
				
			Entry txtPassword = new Entry{
				Placeholder = "Password", 
				Keyboard = Keyboard.Text,
				IsPassword = true, 
			};				
					
			btnLogin.Clicked += async (sender, e) => {
				RestClient client = new RestClient(Globals.baseUrl);
				RestRequest request = new RestRequest(Globals.apiKUrl, Method.GET);

				request.AddParameter("email", txtUsername.Text);
				request.AddParameter("password", txtPassword.Text);

				try {
					var response = await client.Execute<User>(request);
					User current = new User(response.Data);

					Globals.apiKey = current.ApiKey;
					Globals.userId = current.Id;

					await Navigation.PushAsync(new MainPage());

				} catch (Exception) {
					await DisplayAlert("Login Failed", "Username or Password was not found", "Close");	
				}					
			};

			Content = new StackLayout{
				Spacing = 10,
				HorizontalOptions = LayoutOptions.FillAndExpand, 
				VerticalOptions = LayoutOptions.Center,
				Padding = new Thickness(60, 0, 60, 0),
				Children = {
					lblTitle,
					imgMain,
					txtUsername,
					txtPassword, 
					btnLogin
				}
			};
		}

		protected override bool OnBackButtonPressed()
		{
				return true;
		}
	}
}

