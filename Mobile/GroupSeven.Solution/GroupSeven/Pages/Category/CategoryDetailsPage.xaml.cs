﻿using System;
using System.Collections.Generic;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Linq;

namespace GroupSeven
{
	public partial class CategoryDetailsPage : ContentPage
	{
		ObservableCollection<Product> catProducts = new ObservableCollection<Product>();

		public CategoryDetailsPage (Category current)
		{
			InitializeComponent ();

			Title = "Category Details";
			StyleLabel lblName = new StyleLabel ("Category: " + current.Name);
			StyleLabel lblProductList = new StyleLabel("Products listed with " + current.Name +":"); 
			StyleButton btnEdit = new StyleButton ("Edit " + current.Name);
			StyleButton btnDelete = new StyleButton ("Delete " + current.Name);
			StyleButton btnCreateProduct = new StyleButton ("Create New Product");
			SearchBar productSearchBar = new SearchBar{
				Placeholder = "Search Products",
				BackgroundColor = Color.FromHex(Globals.Grayish) 
			};

				foreach (var prod in current.Products) {
					catProducts.Add (new Product {
						ID = prod.ID,	
						Name = prod.Name, 
						CreatedDate = prod.CreatedDate, 
						LastModifiedDate = prod.LastModifiedDate, 
						Price = prod.Price,
						ImageUrl = prod.ImageUrl,
						InventoryCount = prod.InventoryCount, 
						Category = prod.Category, 				
						Manufacturer = prod.Manufacturer
					});
				}

			ListView CategoryProductList = new ListView{
				ItemsSource = catProducts.OrderBy(p=>p.Name).ToList()
			};

			CategoryProductList.ItemTemplate = new DataTemplate (typeof(StyleImageCell));
			CategoryProductList.ItemTemplate.SetBinding (StyleImageCell.ImageSourceProperty, "ImageUrl");
			CategoryProductList.ItemTemplate.SetBinding (StyleImageCell.TextProperty, "Name");

			CategoryProductList.ItemSelected += async (sender, e) => {
				if(e.SelectedItem != null){
					Product item = (Product)e.SelectedItem;
					CategoryProductList.SelectedItem = null;
					await Navigation.PushAsync(new ProductDetailsPage(item));
				}
			};

			btnCreateProduct.Clicked += async (object sender, EventArgs e) => {
				await Navigation.PushAsync(new ProductCreatePage(current, null, null, null));

			};

			btnEdit.Clicked += async (sender, e) => {
				await Navigation.PushAsync(new CategoryEditPage(current));
			};
								
			btnDelete.Clicked += async (sender, e) => {								
				var client = new RestClient(Globals.baseUrl);
				var request = new RestRequest(Globals.apiEditCUrl, Method.DELETE);
				var delete = await DisplayActionSheet("Delete: Are you sure?",null, null, "Delete", "Cancel");
				if(delete == "Delete"){

					//Execute delete function
					request.AddHeader("ApiKey",Globals.apiKey); 
					request.AddHeader("UserId", Globals.userId);
					request.AddParameter("id", current.ID, ParameterType.UrlSegment);

					IRestResponse response = await client.Execute(request);

					await Navigation.PushAsync(new MainPage());
				}
			};

			productSearchBar.TextChanged += (sender, e) => {
				if(string.IsNullOrWhiteSpace(productSearchBar.Text))
					CategoryProductList.ItemsSource = catProducts;
				else
					CategoryProductList.ItemsSource = catProducts.Where(p=>p.Name.ToLower().Contains(productSearchBar.Text.ToLower()));

			};

			productSearchBar.SearchButtonPressed += (sender, e) => {
				if(!string.IsNullOrEmpty(productSearchBar.Text)){
					CategoryProductList.ItemsSource = catProducts.Where(p=>p.Name.ToLower().Contains(productSearchBar.Text.ToLower()));
				}
			};

			Content = new StackLayout { 
				Children = {
					lblName,
					btnEdit,
					btnDelete,
					btnCreateProduct,
					productSearchBar,
					lblProductList,
					CategoryProductList					
				}
			};
		}
	}
}

	

