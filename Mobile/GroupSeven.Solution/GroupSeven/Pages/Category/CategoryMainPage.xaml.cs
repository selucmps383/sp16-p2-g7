﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using RestSharp;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using System.Net;
using System.Linq;

namespace GroupSeven
{
	public partial class CategoryMainPage : ContentPage
	{
		ObservableCollection<Category> categories = new ObservableCollection<Category>();
		private bool isInitialized;

		public CategoryMainPage ()
		{
			InitializeComponent ();

			Title = "Categories";
			Padding = new Thickness(20, Device.OnPlatform(20, 20, 20),20,20);
			StyleButton btnCreateCategory = new StyleButton ("Create New Category");
			SearchBar categorySearchBar = new SearchBar{
				Placeholder = "Search Categories",
				BackgroundColor = Color.FromHex(Globals.Grayish)
			};
	
			ListView CategoryList = new ListView{
				ItemsSource = categories
			};

			CategoryList.ItemTemplate = new DataTemplate (typeof(StyleTextCell));
			CategoryList.ItemTemplate.SetBinding (StyleTextCell.TextProperty, "Name");

			categorySearchBar.TextChanged += (sender, e) => {
				if(string.IsNullOrWhiteSpace(categorySearchBar.Text))
					CategoryList.ItemsSource = categories;
				else
					CategoryList.ItemsSource = categories.Where(p=>p.Name.ToLower().Contains(categorySearchBar.Text.ToLower()));

			};
			categorySearchBar.SearchButtonPressed += (sender, e) => {
				if(!string.IsNullOrEmpty(categorySearchBar.Text)){
					CategoryList.ItemsSource = categories.Where(p=>p.Name.ToLower().Contains(categorySearchBar.Text.ToLower()));
				}
			};
			btnCreateCategory.Clicked += async (sender, e) => {
				await Navigation.PushAsync(new CategoryCreatePage());
			};	

			CategoryList.ItemSelected += async(sender, e) => 
			{

				if (e.SelectedItem != null){
					Category item = (Category)e.SelectedItem;
					CategoryList.SelectedItem = null;

					await Navigation.PushAsync(new CategoryDetailsPage(item));
				}
			};

			Content = new StackLayout{
				Children = {
					categorySearchBar, 
					btnCreateCategory,
					CategoryList
				}
			};
		}

		protected async override void OnAppearing(){
		
			base.OnAppearing ();

			if(isInitialized)
			{
				return;
			}

			var client = new RestClient (Globals.baseUrl);
			var request = new RestRequest(Globals.apiCUrl, Method.GET);

			try {

				var response = await client.Execute<List<Category>>(request);

				List<Category> results = new List<Category>();
				results = response.Data.OrderBy(c=>c.Name).ToList();	

				foreach (var cat in results) {
					categories.Add(new Category{
											ID = cat.ID,	
											Name = cat.Name,
											Products = cat.Products
					});							
				}

			} catch (Exception ex) {
				await DisplayAlert("Error", ex.Message, "Close");			
			}
				
			isInitialized = true;
		
		}

	
	}
}