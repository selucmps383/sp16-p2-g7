﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;

namespace GroupSeven
{
	public partial class CategoryEditPage : ContentPage
	{
		public CategoryEditPage (Category current)
		{
			InitializeComponent ();

			StyleEntryCell txtName = new StyleEntryCell ("Name: ", "Category Name", current.Name);
			StyleButton btnSave = new StyleButton ("Save Changes "); 
			StyleButton btnCancel = new StyleButton ("Cancel");
			TableView CategoryTableView = new TableView{ Intent = TableIntent.Form };

			CategoryTableView.Root = new TableRoot {
				new TableSection (){
					txtName
				} 
			};

			btnSave.Clicked += async (sender, e) => {				

				//Execute save function
				var client = new RestClient(Globals.baseUrl);
				var request = new RestRequest(Globals.apiEditCUrl, Method.PUT);
				request.AddParameter("id", current.ID, ParameterType.UrlSegment);
				request.AddHeader("ApiKey",Globals.apiKey); 
				request.AddHeader("UserId", Globals.userId);
				request.AddBody(new{
					ID = current.ID, 
					Name = txtName.Text
					});

				try {
					IRestResponse response = await client.Execute(request);
				} catch (Exception ex) {
					await DisplayAlert("Error", ex.Message, "Close");
				}

				await Navigation.PushAsync (new MainPage ());			
			};				

			btnCancel.Clicked += async (sender, e) => {
				await Navigation.PushAsync (new MainPage ());
			};

			Content = new StackLayout { 
				Children = {					
					CategoryTableView,
					btnSave, 
					btnCancel
				}
			};
		}
	}
}

