﻿using System;
using System.Collections.Generic;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using Xamarin.Forms;

namespace GroupSeven
{
	public partial class CategoryCreatePage : ContentPage
	{
		public CategoryCreatePage ()
		{
			InitializeComponent ();

			StyleEntryCell txtNewName = new StyleEntryCell ("Name: ","Category Name", "" );
			StyleButton btnSave = new StyleButton ("Save Changes");
			StyleButton btnCancel = new StyleButton ("Cancel");
			TableView CategoryTableView = new TableView{ Intent = TableIntent.Form };

			CategoryTableView.Root = new TableRoot {
				new TableSection (){
					txtNewName
				} 
			};
				
			btnSave.Clicked += async (sender, e) => {				
				var client = new RestClient(Globals.baseUrl);
				var request = new RestRequest(Globals.apiCUrl, Method.POST);

				request.AddHeader("ApiKey",Globals.apiKey); 
				request.AddHeader("UserId", Globals.userId);
				request.AddParameter("Name", txtNewName.Text);					

				//Execute save function
				try {
					IRestResponse response = await client.Execute(request);

				} catch (Exception ex) {
					await DisplayAlert("Error", ex.Message, "Close");
				}

				await Navigation.PushAsync(new MainPage());			
			};

			btnCancel.Clicked += async (sender, e) => {
				await Navigation.PushAsync(new MainPage());
			};

			Content = new StackLayout {
				Children = {
					CategoryTableView, 
					btnSave, 
					btnCancel
				}
			};
		}
	}
}

