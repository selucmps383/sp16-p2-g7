﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;

namespace GroupSeven
{
	public partial class ProductDetailsPage : ContentPage
	{

		public ProductDetailsPage ()
		{
			InitializeComponent ();
		}
		public ProductDetailsPage (Product current)
		{
			InitializeComponent ();

			Image imgProduct = new Image ();
			if (current.ImageUrl == null || current.ImageUrl == "")
				imgProduct.Source = ImageSource.FromUri (new Uri (ImageSources.imgNotAvailable));
			else
				imgProduct.Source = ImageSource.FromUri(new Uri(current.ImageUrl));
			
			StyleLabel lblName = new StyleLabel (("Product: " + current.Name));
			StyleLabel lblPrice = new StyleLabel (("Price: $" + current.Price.ToString()));
			StyleLabel lblInventoryCount = new StyleLabel (("Inventory Count: " + current.InventoryCount.ToString()));
			StyleLabel lblCategory= new StyleLabel (("Category: " + current.Category.Name));
			StyleLabel lblManufacturer = new StyleLabel (("Manufacturer: " + current.Manufacturer.Name));
			StyleLabel lblCreatedDate = new StyleLabel (("Created: " + current.CreatedDate.Month +"/"+  current.CreatedDate.Day +"/"+ current.CreatedDate.Year));
			StyleLabel lblLastModifiedDate = new StyleLabel (("Last Modified On: " + current.LastModifiedDate));

			StyleButton btnDelete = new StyleButton ("Delete " + current.Name);
			StyleButton btnEdit = new StyleButton ("Edit " + current.Name);

			btnEdit.Clicked += async (sender, e) => {
				
				await Navigation.PushAsync(new ProductEditPage(current, null));
			};

			btnDelete.Clicked += async (sender, e) => {							
				var delete = await DisplayActionSheet("Delete: Are you sure?",null, null, "Delete", "Cancel");
				var client = new RestClient (Globals.baseUrl);
				var request = new RestRequest(Globals.apiEditPUrl, Method.DELETE);

				request.AddHeader("ApiKey",Globals.apiKey); 
				request.AddHeader("UserId", Globals.userId);
				request.AddParameter("id", current.ID, ParameterType.UrlSegment);
					if(delete == "Delete"){
					try {
						//Execute delete function
						IRestResponse response = await client.Execute(request);

					} catch (Exception ex) {
						await DisplayAlert("Error", ex.Message, "Close");
					}

					await Navigation.PushAsync(new MainPage());
				}
			};

			Content = new StackLayout { 
				Children = {
					imgProduct,
					lblName,
					lblPrice, 
					lblInventoryCount, 
					lblManufacturer, 
					lblCategory,
					lblCreatedDate, 
					lblLastModifiedDate,
					btnEdit,
					btnDelete
				}
			};
		}
	}
}

