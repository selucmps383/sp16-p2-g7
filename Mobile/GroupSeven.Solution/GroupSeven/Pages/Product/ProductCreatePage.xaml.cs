﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using System.Linq;

namespace GroupSeven
{
	public partial class ProductCreatePage : ContentPage
	{

		public ProductCreatePage (Category cat, Manufacturer man, string imageSource, Product current)
		{

			InitializeComponent ();				

			var client = new RestClient (Globals.baseUrl);
			//imageSource = null;

			Category selectedCategory = new Category ();
			Manufacturer selectedManufacturer = new Manufacturer();
			if (cat != null) {
				selectedCategory = cat;
			} 
			if(man != null) {
				selectedManufacturer = man;
			} 
			if (current != null) {
				selectedCategory = current.Category;
				selectedManufacturer = current.Manufacturer;
			}
			string txtCategoryName = ((cat == null) ? "None " : ((current == null) ? cat.Name : ((current.Category.Name == null) ? "None" : current.Category.Name)));
			string txtManufacturerName = ((man == null) ? "None " : ((current == null) ? man.Name : ((current.Manufacturer.Name == null) ? "None" : current.Manufacturer.Name)));
			string currentImage = ((imageSource == null) ? ImageSources.imgNotAvailable : imageSource);
			string currentName = ((current == null) ? "" : ((current.Name == null ) ? "" : current.Name));
			string currentPrice = ((current == null) ? "" : ((current.Price.ToString() == null ) ? "" : current.Price.ToString()));
			string currentInventoryCount =((current == null) ? "" : ((current.InventoryCount.ToString() == null ) ? "" : current.InventoryCount.ToString()));

			//string currentImage = ImageSources.imgNotAvailable;
			StyleLabel lblNewManufacturer = new StyleLabel ("Manufacturer: ");	
			StyleLabel lblNewManufacturerName = new StyleLabel (txtManufacturerName){HorizontalOptions = LayoutOptions.StartAndExpand};
			StyleLabel lblNewCategory = new StyleLabel ("Category: ");	
			StyleLabel lblNewCategoryName = new StyleLabel (txtCategoryName){HorizontalOptions = LayoutOptions.StartAndExpand};
			StyleLabel lblImage = new StyleLabel ("Image: ");
			Image imgProduct = new Image (){HorizontalOptions = LayoutOptions.StartAndExpand, Source = ImageSource.FromUri(new Uri(currentImage))};
			StyleButton btnManufacturerSelect = new StyleButton ("Edit");
			StyleButton btnCategorySelect = new StyleButton ("Edit");
			StyleButton btnImageSelect = new StyleButton ("Edit");
			StyleButton btnCancel = new StyleButton ("Cancel");
			StyleButton btnSave = new StyleButton ("Save");

			StyleEntryCell txtProductName = new StyleEntryCell ("Name: ", "Product Name", currentName );
			StyleEntryCell txtProductPrice = new StyleEntryCell ("Price: $", "Product Price", currentPrice);
			StyleEntryCell txtProductInventoryCount = new StyleEntryCell ("Inventory Count: ", "Product Starting Quantity", currentInventoryCount);

			TableView productTableView = new TableView{ };
			productTableView.Intent = TableIntent.Form;

			var manufacturerLayout = new StackLayout (){ Orientation = StackOrientation.Horizontal,

				Padding = new Thickness(10, 0, 10, 0),
				Children = {
					lblNewManufacturer, 
					lblNewManufacturerName, 
					btnManufacturerSelect
				}
			};

			var categoryLayout = new StackLayout () {
				Orientation = StackOrientation.Horizontal, 
				Padding = new Thickness(10, 0, 10, 0),				
				Children = {
					lblNewCategory, 
					lblNewCategoryName, 
					btnCategorySelect
				}
			};
			var imageLayout = new StackLayout () {
				Orientation = StackOrientation.Horizontal, 
				Padding = new Thickness(10, 0, 10, 0),				
				Children = {
					lblImage, 
					imgProduct, 
					btnImageSelect
				}
			};


			productTableView.Root = new TableRoot {
				new TableSection() {					
					txtProductName,
					txtProductPrice, 
					txtProductInventoryCount,
					new ViewCell (){ View = manufacturerLayout }, 
					new ViewCell (){ View = categoryLayout },
					new ViewCell (){ View = imageLayout }
				}
			};

			btnManufacturerSelect.Clicked += async (sender, e) => {
				RestRequest ManufacturerRequest = new RestRequest(Globals.apiMUrl, Method.GET);

				var response = await client.Execute<List<Manufacturer>>(ManufacturerRequest);

				List<Manufacturer> results = new List<Manufacturer>();
				results = response.Data.OrderBy(m=>m.Name).ToList();
				List<string> manNamesList = new List<string>();

				foreach (var manufacturer in results) {
					manNamesList.Add(manufacturer.Name);
				}
				var manNamesArray = manNamesList.ToArray();
				var manSelect = await DisplayActionSheet("Manufacturers: ", null, null, manNamesArray);

				if(manSelect != null){
					selectedManufacturer = results.Find(m=>m.Name == manSelect);
					lblNewManufacturerName.Text = selectedManufacturer.Name;
				}					
			};
				
			btnCategorySelect.Clicked += async (sender, e) => {
				RestRequest CategoryRequest = new RestRequest(Globals.apiCUrl, Method.GET);
				var response = await client.Execute<List<Category>>(CategoryRequest);

				List<Category> results = new List<Category>();

				results = response.Data.OrderBy(c=>c.Name).ToList();

				List<string> catNamesList = new List<string>();

				foreach (var category in results) {
					catNamesList.Add(category.Name);
				}

				var catNamesArray = catNamesList.ToArray();
				var catSelect = await DisplayActionSheet("Categories: ", null, null, catNamesArray);
				if(catSelect != null){					
					selectedCategory = results.Find(c=>c.Name == catSelect);
					lblNewCategoryName.Text = selectedCategory.Name;
				}
			};

			btnImageSelect.Clicked += async (sender, e) => {
				Product copy = new Product();
				if(txtProductName.Text != "")
					copy.Name = txtProductName.Text; 
				if(current != null)
					copy.CreatedDate = DateTime.Now.Date; 
				if(txtProductPrice.Text != "")
					copy.Price = Decimal.Parse(txtProductPrice.Text);
				if(txtProductInventoryCount.Text != "")
					copy.InventoryCount = Int32.Parse(txtProductInventoryCount.Text);
				if(imageSource != null)
					copy.ImageUrl = currentImage;
				if(selectedCategory != null)
					copy.Category = selectedCategory;
				if(selectedManufacturer != null)
					copy.Manufacturer = selectedManufacturer;
				await Navigation.PushAsync(new ImageListPage(cat, man, copy));

			};


			btnSave.Clicked += async (sender, e) => {					
				//Execute save function
				var request = new RestRequest(Globals.apiPUrl, Method.POST);
				try {
				request.AddHeader("ApiKey",Globals.apiKey); 
				request.AddHeader("UserId", Globals.userId);
				request.AddBody(new Product(){
					Name = txtProductName.Text, 
					CreatedDate = DateTime.Now.Date, 
					LastModifiedDate = DateTime.UtcNow, 
					Price = decimal.Parse(txtProductPrice.Text), 
					InventoryCount = Int32.Parse(txtProductInventoryCount.Text),
					ImageUrl = imageSource,
					Category = selectedCategory, 
					Manufacturer = selectedManufacturer
				});

					IRestResponse<Product> response = await client.Execute<Product>(request);
					await Navigation.PushAsync(new MainPage());	
				} catch (Exception ex) {
					await DisplayAlert("Error", ex.Message, "Close");
				}		
			};

			btnCancel.Clicked += async (sender, e) => {
				await Navigation.PushAsync(new MainPage());
			};

			Content = new StackLayout {
				Children = {
					productTableView,
					btnImageSelect,
					btnSave,
					btnCancel
				}
			};
		}

	}
}

