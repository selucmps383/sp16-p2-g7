﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using System.Linq;

namespace GroupSeven
{
	public partial class ProductEditPage : ContentPage
	{
		public ProductEditPage(Product current, string imageUrl)
		{
			InitializeComponent ();

			var client = new RestClient (Globals.baseUrl);
			if (imageUrl == null && current.ImageUrl == null)
				imageUrl = ImageSources.imgDog;
			if (imageUrl == null && current.ImageUrl != null)
				imageUrl = current.ImageUrl;
			Category selectedCategory = current.Category;
			Manufacturer selectedManufacturer = current.Manufacturer;
			StyleLabel lblManufacturer = new StyleLabel ("Manufacturer: ");
			StyleLabel lblManufacturerName = new StyleLabel (current.Manufacturer.Name){HorizontalOptions = LayoutOptions.StartAndExpand};
			StyleLabel lblCategory = new StyleLabel ("Category: ");
			StyleLabel lblCategoryName = new StyleLabel (current.Category.Name){HorizontalOptions = LayoutOptions.StartAndExpand};
			StyleLabel lblImage = new StyleLabel ("Image: ");
			Image imgProduct = new Image{Source = ImageSource.FromUri(new Uri(((imageUrl == "") ? ImageSources.imgNotAvailable : imageUrl))), HorizontalOptions = LayoutOptions.StartAndExpand };
			StyleButton btnManufacturerSelect = new StyleButton ("Edit");
			StyleButton btnCategorySelect = new StyleButton ("Edit");
			StyleButton btnCancel = new StyleButton ("Cancel");
			StyleButton btnSave = new StyleButton ("Save");
			StyleButton btnImageSelect = new StyleButton ("Edit");
			TableView productTableView = new TableView{};
			productTableView.Intent = TableIntent.Form;

				var manufacturerLayout = new StackLayout (){ Orientation = StackOrientation.Horizontal,
				
				Padding = new Thickness(10, 0, 10, 0),
					Children = {
						lblManufacturer, 
						lblManufacturerName, 
						btnManufacturerSelect
					}
				};

				var categoryLayout = new StackLayout () {
					Orientation = StackOrientation.Horizontal, 
				Padding = new Thickness(10, 0, 10, 0),				
				Children = {
						lblCategory, 
						lblCategoryName, 
						btnCategorySelect
					}
				};
			var imageLayout = new StackLayout () {
				Orientation = StackOrientation.Horizontal, 
				Padding = new Thickness(10, 0, 10, 0),				
				Children = {
					lblImage, 
					imgProduct, 
					btnImageSelect
				}
			};

			StyleEntryCell txtProductName = new StyleEntryCell ("Name: ", "Product Name", current.Name);
			StyleEntryCell txtProductPrice = new StyleEntryCell ("Price: $", "Product Price", current.Price.ToString ());
			StyleEntryCell txtProductInventoryCount = new StyleEntryCell ("Inventory Count: ", "Product Starting Quantity", current.InventoryCount.ToString());

			productTableView.Root = new TableRoot {
				new TableSection() {					
						txtProductName,
						txtProductPrice, 
						txtProductInventoryCount,
						new ViewCell (){ View = manufacturerLayout }, 
						new ViewCell (){ View = categoryLayout },
						new ViewCell(){View = imageLayout}
						

					}

				};

			btnManufacturerSelect.Clicked += async (sender, e) => {
				RestRequest ManufacturerRequest = new RestRequest(Globals.apiMUrl, Method.GET);
				var response = await client.Execute<List<Manufacturer>>(ManufacturerRequest);
				List<Manufacturer> results = new List<Manufacturer>();
				results = response.Data.OrderBy(m=>m.Name).ToList();
				List<string> manNamesList = new List<string>();

				foreach (var man in results) {
					manNamesList.Add(man.Name);
				}
				var manNamesArray = manNamesList.ToArray();

				var manSelect = await DisplayActionSheet("Manufacturers: ", null, null, manNamesArray);
				if(manSelect != null){
					selectedManufacturer = results.Find(m=>m.Name == manSelect);
					lblManufacturerName.Text = selectedManufacturer.Name;
				}
			};				
		
			btnCategorySelect.Clicked += async (sender, e) => {
				RestRequest CategoryRequest = new RestRequest(Globals.apiCUrl, Method.GET);
				var response = await client.Execute<List<Category>>(CategoryRequest);

			List<Category> results = new List<Category>();

				results = response.Data.OrderBy(c=>c.Name).ToList();
				List<string> catNamesList = new List<string>();

				foreach (var cat in results) {
					catNamesList.Add(cat.Name);
				}

				var catNamesArray = catNamesList.ToArray();
				var catSelect = await DisplayActionSheet("Categories: ", null, null, catNamesArray);
				if(catSelect != null){

					selectedCategory = results.Find(c=>c.Name == catSelect);
					lblCategoryName.Text = selectedCategory.Name;
				}
			};
			btnImageSelect.Clicked += async (sender, e) => {
				

				await Navigation.PushAsync(new ImageListPage(current));


			};
		btnSave.Clicked += async (sender, e) => {				
				//Execute save function
				try {					
					var request = new RestRequest(Globals.apiEditPUrl, Method.PUT);

					request.AddParameter("id", current.ID, ParameterType.UrlSegment);
					request.AddHeader("ApiKey",Globals.apiKey); 
					request.AddHeader("UserId", Globals.userId);
					request.AddBody(
						new Product(){
						ID= current.ID, 
						Name = txtProductName.Text,
						CreatedDate = current.CreatedDate.Date,					
						LastModifiedDate = DateTime.UtcNow, 
						Price = decimal.Parse(txtProductPrice.Text), 
						InventoryCount = Int32.Parse(txtProductInventoryCount.Text),
						ImageUrl = imageUrl,
						Category = selectedCategory,
						Manufacturer = selectedManufacturer,					
					});
					IRestResponse response = await client.Execute(request);
					await Navigation.PushAsync(new MainPage());		
				} catch (Exception ex) {
					await DisplayAlert("Error", ex.Message, "Close");

				}

					
			};

			btnCancel.Clicked += async (sender, e) => {
				await Navigation.PushAsync(new MainPage());
			};

		Content = new StackLayout { 				
			Children = {
				productTableView,				
				btnSave, 
				btnCancel				
				}
			};
		}
	}
}

