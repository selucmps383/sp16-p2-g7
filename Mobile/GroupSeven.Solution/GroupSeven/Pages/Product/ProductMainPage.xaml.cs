﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using RestSharp;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using System.Net;
using System.Linq;

namespace GroupSeven
{
	public partial class ProductMainPage : ContentPage
	{
		ObservableCollection<Product> products = new ObservableCollection<Product> ();
		private bool isInitialized;

		public ProductMainPage ()
		{
			InitializeComponent ();

			Title = "Products";
			Padding = new Thickness(20, Device.OnPlatform(20, 20, 20),20,20);

			StyleButton btnCreateProduct = new StyleButton ("Create New Product");
			SearchBar productSearchBar = new SearchBar{
				Placeholder = "Search Products",
				BackgroundColor = Color.FromHex(Globals.Grayish) 
			};

			ListView ProductList = new ListView{
				ItemsSource = products
			};

			ProductList.ItemTemplate = new DataTemplate (typeof(StyleImageCell));
			ProductList.ItemTemplate.SetBinding (StyleImageCell.ImageSourceProperty, "ImageUrl");
			ProductList.ItemTemplate.SetBinding (StyleImageCell.TextProperty, "Name");

			btnCreateProduct.Clicked += async (sender, e) => {
				await Navigation.PushAsync(new ProductCreatePage(null, null, null, null));

				};				
		
			ProductList.ItemSelected += async(sender, e) => 
			{
				if (e.SelectedItem != null)
				{		
					
					Product item = (Product)e.SelectedItem;
					ProductList.SelectedItem = null;
					await Navigation.PushAsync(new ProductDetailsPage(item));
				}
			};
			productSearchBar.TextChanged += (sender, e) => {
				if(string.IsNullOrWhiteSpace(productSearchBar.Text))
					ProductList.ItemsSource = products;
				else
					ProductList.ItemsSource = products.Where(p=>p.Name.ToLower().Contains(productSearchBar.Text.ToLower()));
			
			};
			productSearchBar.SearchButtonPressed += (sender, e) => {
				if(!string.IsNullOrEmpty(productSearchBar.Text)){
					ProductList.ItemsSource = products.Where(p=>p.Name.ToLower().Contains(productSearchBar.Text.ToLower()));
				}
			};
	
			Content = new StackLayout{
				Children = {					
					productSearchBar,
					btnCreateProduct,
					ProductList, 				
				}
			};
		}

		protected async override void OnAppearing()
		{
			base.OnAppearing ();

			if(isInitialized)
			{
				return;
			}

			var client = new RestClient (Globals.baseUrl);
			var request = new RestRequest(Globals.apiPUrl, Method.GET);

			try {
				var response = await client.Execute<List<Product>>(request);

				List<Product> results = new List<Product>();
				results = response.Data.OrderBy(p=>p.Name).ToList();	

				foreach (var prod in results) {
					products.Add(new Product(){
												ID = prod.ID,	
												Name = prod.Name, 
												CreatedDate = prod.CreatedDate, 
												LastModifiedDate = prod.LastModifiedDate, 
												Price = prod.Price, 
												ImageUrl = prod.ImageUrl,
												InventoryCount = prod.InventoryCount, 
												Category = prod.Category, 
												Manufacturer = prod.Manufacturer}) ;							
				}

			} catch (Exception ex) {				
				await DisplayAlert("Error", ex.Message, "Close");			
			}
				
			isInitialized = true;
		}			
	}
}

