﻿using System;

using Xamarin.Forms;
using System.Collections.Generic;

namespace GroupSeven
{
	public class ImageListPage : ContentPage
	{
		List<StyleImageCell> images = new List<StyleImageCell> {			
			new StyleImageCell(ImageSources.imgChameleon, "Chameleon"),
			new StyleImageCell(ImageSources.imgCereal, "Cereal"),
			new StyleImageCell(ImageSources.imgChocolate, "Chocolate"),
			new StyleImageCell(ImageSources.imgDog, "Doggy"),
			new StyleImageCell(ImageSources.imgEggs, "Eggs"),
			new StyleImageCell(ImageSources.imgGummies, "Gummy Bears"),
			new StyleImageCell(ImageSources.imgIce, "Ice"),
			new StyleImageCell(ImageSources.imgIceCream, "Ice Cream"),
			new StyleImageCell(ImageSources.imgLollipop, "Lollipop"),
			new StyleImageCell(ImageSources.imgPikachu, "Pikachu"),
			new StyleImageCell(ImageSources.imgSandwich, "Sandwich"),
			new StyleImageCell(ImageSources.imgSnickers, "Snickers"),
			new StyleImageCell(ImageSources.imgSmoothie, "Smoothie"),
			new StyleImageCell(ImageSources.imgTiger, "Sabertooth Tiger"),
			new StyleImageCell(ImageSources.imgWrench, "Wrench"),
		};

		public ImageListPage (Product current)
		{
			ListView ImageList = new ListView{
				ItemsSource = images

			};
			ImageList.ItemTemplate = new DataTemplate (typeof(StyleImageCell));
			ImageList.ItemTemplate.SetBinding (StyleImageCell.ImageSourceProperty, "ImageSource");
			ImageList.ItemTemplate.SetBinding (StyleImageCell.TextProperty, "Text");

			ImageList.ItemSelected += async (sender, e) => {
				StyleImageCell selectedImage= (StyleImageCell)e.SelectedItem;

				string selectedImageUrl = selectedImage.ImageSource.GetValue(UriImageSource.UriProperty).ToString();

				await Navigation.PushAsync(new ProductEditPage(current, selectedImageUrl));			

			};

			Content = new StackLayout { 
				Children = {
					ImageList
				}
			};
		}


		public ImageListPage (Category cat, Manufacturer man, Product current)
		{
			
			ListView ImageList = new ListView{
				ItemsSource = images

			};
			ImageList.ItemTemplate = new DataTemplate (typeof(StyleImageCell));
			ImageList.ItemTemplate.SetBinding (StyleImageCell.ImageSourceProperty, "ImageSource");
			ImageList.ItemTemplate.SetBinding (StyleImageCell.TextProperty, "Text");

			ImageList.ItemSelected += async (sender, e) => {
				StyleImageCell selectedImage= (StyleImageCell)e.SelectedItem;
				string selectedImageUrl = selectedImage.ImageSource.GetValue(UriImageSource.UriProperty).ToString();
			
				await Navigation.PushAsync(new ProductCreatePage(current.Category, current.Manufacturer, selectedImageUrl, current));				
			};

			Content = new StackLayout { 
				Children = {
					ImageList
				}
			};
		}
	}
}


