﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;

namespace GroupSeven
{
	public partial class ManufacturerCreatePage : ContentPage
	{
		public ManufacturerCreatePage ()
		{
			InitializeComponent ();

			StyleEntryCell txtNewName = new StyleEntryCell ("Name: ", "Manufacturer Name","");
			StyleButton btnSave = new StyleButton ("Save");
			StyleButton btnCancel = new StyleButton ("Cancel");
			TableView ManufacturerTableView = new TableView{ Intent = TableIntent.Form };

			ManufacturerTableView.Root = new TableRoot {
				new TableSection (){
					txtNewName 
				} 
			};

			btnSave.Clicked += async (sender, e) => {				
				//Execute save function
				var client = new RestClient(Globals.baseUrl);
				var request = new RestRequest(Globals.apiMUrl, Method.POST);

				request.AddHeader("ApiKey",Globals.apiKey); 
				request.AddHeader("UserId", Globals.userId);
				request.AddParameter("Name", txtNewName.Text);

				try {
					IRestResponse response = await client.Execute(request);

				} catch (Exception ex) {
					await DisplayAlert("Error", ex.Message, "Close");
				}

				await Navigation.PushAsync(new MainPage());			
			};

			btnCancel.Clicked += async (sender, e) => {
				await Navigation.PushAsync(new MainPage());
			};

			Content = new StackLayout {
				Children = {
					ManufacturerTableView,
					btnSave, 
					btnCancel
				}
			};

		}
	}
}

