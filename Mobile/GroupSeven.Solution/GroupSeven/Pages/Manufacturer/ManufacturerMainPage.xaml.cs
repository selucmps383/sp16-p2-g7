﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using RestSharp;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using System.Net;
using System.Linq;

namespace GroupSeven
{
	public partial class ManufacturerMainPage : ContentPage
	{
		ObservableCollection<Manufacturer> manufacturers = new ObservableCollection<Manufacturer>();
		private bool isInitialized;

		public ManufacturerMainPage ()
		{
			InitializeComponent ();

			Title = "Manufacturers";
			Padding = new Thickness(20, Device.OnPlatform(20, 20, 20),20,20);

			StyleButton btnCreateManufacturer = new StyleButton ("Create New Manufacturer");
			SearchBar manufacturerSearchBar = new SearchBar{
				Placeholder = "Search Manufacturers",
				BackgroundColor = Color.FromHex(Globals.Grayish)
			};

			ListView ManufacturerList = new ListView{
				ItemsSource = manufacturers
			};

			ManufacturerList.ItemTemplate = new DataTemplate (typeof(StyleTextCell));
			ManufacturerList.ItemTemplate.SetBinding (StyleTextCell.TextProperty, "Name");

			ManufacturerList.ItemsSource = manufacturers;

			btnCreateManufacturer.Clicked += async (sender, e) => {
				await Navigation.PushAsync(new ManufacturerCreatePage());

			};	

			manufacturerSearchBar.TextChanged += (sender, e) => {
				if(string.IsNullOrWhiteSpace(manufacturerSearchBar.Text))
					ManufacturerList.ItemsSource = manufacturers;
				else
					ManufacturerList.ItemsSource = manufacturers.Where(p=>p.Name.ToLower().Contains(manufacturerSearchBar.Text.ToLower()));
			};

			manufacturerSearchBar.SearchButtonPressed += (sender, e) => {
				if(!string.IsNullOrEmpty(manufacturerSearchBar.Text)){
					ManufacturerList.ItemsSource = manufacturers.Where(p=>p.Name.ToLower().Contains(manufacturerSearchBar.Text.ToLower()));
				}
			};

			ManufacturerList.ItemSelected += async(sender, e) => 
			{
				if (e.SelectedItem != null)
				{
					Manufacturer item = (Manufacturer)e.SelectedItem;
					ManufacturerList.SelectedItem = null;
					await Navigation.PushAsync(new ManufacturerDetailsPage(item));
				}
			};

			Content = new StackLayout{
				Children = {
					manufacturerSearchBar,
					btnCreateManufacturer,
					ManufacturerList
				}
			};
		}

		protected async override void OnAppearing(){
		
			base.OnAppearing ();

			if(isInitialized){return;}

			var client = new RestClient (Globals.baseUrl);
			var request = new RestRequest (Globals.apiMUrl, Method.GET);
		

			try {
				var response = await client.Execute<List<Manufacturer>>(request);

				List<Manufacturer> results = new List<Manufacturer>();
				results = response.Data.OrderBy(m=>m.Name).ToList();
			
				foreach (var man in results) {
					manufacturers.Add(new Manufacturer{
													ID = man.ID, 
													Name = man.Name, 
													Products = man.Products});
				}
			} catch (Exception ex) {
				await DisplayAlert ("Error", ex.Message, "Close");				
			}

			isInitialized = true;					
		}			
	}
}

