﻿using System;
using System.Collections.Generic;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Linq;

namespace GroupSeven
{
	public partial class ManufacturerDetailsPage : ContentPage
	{
		ObservableCollection<Product> manProducts = new ObservableCollection<Product>();

		public ManufacturerDetailsPage (Manufacturer current)
		{
			InitializeComponent ();

			StyleLabel lblName = new StyleLabel ("Manufacturer: " + current.Name);
			StyleLabel lblProductList = new StyleLabel ("Products listed with " + current.Name + ":");
			StyleButton btnEdit = new StyleButton ("Edit " + current.Name);
			StyleButton btnDelete = new StyleButton ("Delete " + current.Name);
			StyleButton btnCreateProduct = new StyleButton ("Create New Product");
			SearchBar productSearchBar = new SearchBar{
				Placeholder = "Search Products",
				BackgroundColor = Color.FromHex(Globals.Grayish) 
			};

			btnCreateProduct.Clicked += async (object sender, EventArgs e) => {
				await Navigation.PushAsync(new ProductCreatePage(null, current, null, null));

			};

			btnEdit.Clicked += async (sender, e) => {
				await Navigation.PushAsync(new ManufacturerEditPage(current));
			};

			btnDelete.Clicked += async (sender, e) => {				
				var delete = await DisplayActionSheet("Delete: Are you sure?",null, null, "Delete", "Cancel");

				if(delete == "Delete"){
					//Execute delete function
					try {
						var client = new RestClient(Globals.baseUrl);
						var request = new RestRequest(Globals.apiEditMUrl, Method.DELETE);

						request.AddHeader("ApiKey",Globals.apiKey); 
						request.AddHeader("UserId", Globals.userId);
						request.AddParameter("id", current.ID, ParameterType.UrlSegment);

						IRestResponse response = await client.Execute(request);

					} catch (Exception ex) {
						await DisplayAlert("Error", ex.Message, "Close");
					}

					await Navigation.PushAsync(new MainPage());
				}
			};

			foreach (var prod in current.Products) {
				manProducts.Add (new Product {
					ID = prod.ID,	
					Name = prod.Name, 
					CreatedDate = prod.CreatedDate, 
					LastModifiedDate = prod.LastModifiedDate, 
					Price = prod.Price, 
					ImageUrl = prod.ImageUrl,
					InventoryCount = prod.InventoryCount, 
					Category = prod.Category, 

					Manufacturer = prod.Manufacturer
				});
			}

			ListView ManufacturerProductList = new ListView{
				ItemsSource = manProducts.OrderBy(p=>p.Name).ToList()

			};

			ManufacturerProductList.ItemTemplate = new DataTemplate (typeof(StyleImageCell));
			ManufacturerProductList.ItemTemplate.SetBinding (StyleImageCell.ImageSourceProperty, "ImageUrl");
			ManufacturerProductList.ItemTemplate.SetBinding (StyleImageCell.TextProperty, "Name");

			ManufacturerProductList.ItemSelected += async (sender, e) => {
					if(e.SelectedItem != null){
						Product item = (Product)e.SelectedItem;
						ManufacturerProductList.SelectedItem = null;
						await Navigation.PushAsync(new ProductDetailsPage(item));
					}
				};

			productSearchBar.TextChanged += (sender, e) => {
				if(string.IsNullOrWhiteSpace(productSearchBar.Text))
					ManufacturerProductList.ItemsSource = manProducts;
				else
					ManufacturerProductList.ItemsSource = manProducts.Where(p=>p.Name.ToLower().Contains(productSearchBar.Text.ToLower()));

			};
			productSearchBar.SearchButtonPressed += (sender, e) => {
				if(!string.IsNullOrEmpty(productSearchBar.Text))
					ManufacturerProductList.ItemsSource = manProducts.Where(p=>p.Name.ToLower().Contains(productSearchBar.Text.ToLower()));				
			};

			Content = new StackLayout { 
				Children = {
					lblName,
					btnEdit,
					btnDelete, 
					btnCreateProduct,
					productSearchBar,
					lblProductList,			
					ManufacturerProductList
				}
			};
		}
	}
}

