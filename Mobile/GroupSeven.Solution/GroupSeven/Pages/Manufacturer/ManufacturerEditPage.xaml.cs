﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;

namespace GroupSeven
{
	public partial class ManufacturerEditPage : ContentPage
	{
		public ManufacturerEditPage (Manufacturer current)
		{
			InitializeComponent ();

			StyleEntryCell txtName = new StyleEntryCell ("Name: ", "Manufacturer Name", current.Name);
			StyleButton btnSave = new StyleButton ("Save");
			StyleButton btnCancel = new StyleButton ("Cancel");
			TableView ManufacturerTableView = new TableView{ Intent = TableIntent.Form };

			ManufacturerTableView.Root = new TableRoot {
				new TableSection (){
					txtName
				} 
			};

			btnSave.Clicked += async (sender, e) => {				
				//Execute save function
				try{
					var client = new RestClient(Globals.baseUrl);
					var request = new RestRequest(Globals.apiEditMUrl, Method.PUT);
				
					request.AddParameter("id", current.ID, ParameterType.UrlSegment);
					request.AddHeader("ApiKey",Globals.apiKey); 
					request.AddHeader("UserId", Globals.userId);
					request.AddBody(new{
					ID = current.ID, 
					Name = txtName.Text
				});

				IRestResponse response = await client.Execute(request);
				} catch(Exception ex){
					await DisplayAlert("Error", ex.Message, "Close");
				}

				await Navigation.PushAsync(new MainPage());			
			};
				
			btnCancel.Clicked += async (sender, e) => {
				await Navigation.PushAsync(new MainPage());
			};

			Content = new StackLayout { 
				Children = {
					ManufacturerTableView,
					btnSave, 
					btnCancel
				}
			};
		}
	}
}

