﻿using System;

namespace GroupSeven
{
	public static class Globals
	{
		
		//public const string baseUrl ="http://192.168.1.127:10194";
		public const string baseUrl = "http://147.174.173.49:10194";
		public static string apiKey;
		public static int userId;
		public const string apiKUrl ="/api/ApiKey";
		public const string apiPUrl = "/api/Products";
		public const string apiMUrl = "/api/Manufacturers";
		public const string apiCUrl = "/api/Categories";
		public const string apiEditPUrl = "/api/Products/{id}";
		public const string apiEditMUrl = "/api/Manufacturers/{id}";
		public const string apiEditCUrl = "/api/Categories/{id}";

		//Custom Colors
		public static string Grayish = "353432";
		public static string Purplish = "9010e0";
		public static string Aquaish = "0ED0E6";
		public static string Blackish = "000000";



	}
}

