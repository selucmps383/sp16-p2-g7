﻿using System;

namespace GroupSeven
{
	public static class ImageSources
	{
		
		public const string imgPikachu = "https://upload.wikimedia.org/wikipedia/uk/a/a2/025-Pikachu.jpg";
		public const string imgCereal = "http://www.extension.uidaho.edu/diabetesplate/food/img/starch_cereal.jpg";
		public const string imgTiger = "http://animalstime.com/wp-content/uploads/2012/04/sabre_toothed_tiger-150x150.jpg";
		public const string imgIceCream = "http://pensieve.typepad.com/pensieve/images/2007/08/12/chocolate_ice_cream.jpg";
		public const string imgEggs = "http://www.foodsafety.gov/images/eggs2.jpg";
		public const string imgChocolate = "https://guiadecontactomedico.files.wordpress.com/2011/09/chocolate-amrgo.jpg";
		public const string imgDog = "http://dogbreedsinfo.org/images/Chihuahua_Puppy.jpg";
		public const string imgSandwich = "http://thumbs.dreamstime.com/t/ham-sub-sandwich-white-11685094.jpg";
		public const string imgIce = "http://processspecialist.com/increasesales/wp-content/uploads/2012/06/Ice-MS-00.jpg";
		public const string imgSmoothie = "http://www.hetmentaledieetplan.com/wp-content/uploads/2011/05/groene-smoothie.jpg";
		public const string imgJohn = "http://staff-look-a-likes.synthasite.com/resources/john%20candy.jpg";
		public const string imgNotAvailable = "http://clubecandoca.com.br/images/vector-cone-i4.jpg";
		public const string imgWrench = "http://www.supplyairtools.com/pro_small/wrench/monkey_wrench.jpg";
		public const string imgChameleon = "http://thumb101.shutterstock.com/photos/thumb_large/349087/171899567.jpg";
		public const string imgSnickers = "http://wildo.ru/wp-content/uploads/2011/03/Snickers.jpg";
		public const string imgGummies = "http://partystock.ca/media/catalog/product/cache/1/small_image/178x/9df78eab33525d08d6e5fb8d27136e95/g/u/gummi_bears.jpg";
		public const string imgHammer ="https://archadeckofcentralsouthcarolina.files.wordpress.com/2015/01/golden-hammer.jpg";
		public const string imgLollipop ="http://www.29thstreetdesigns.com/item_images/Bir4_PinkLollipop_Thumbsmall.jpg";
		public const string imgChocolateHammer = "http://www.burstschocolates.com/var/images/product/300.300/459.jpg";
		public const string imgShovel = "http://images-en.busytrade.com/154128400/Sell-Shovel-spade.jpg";
		public const string imgScrewdriver = "http://www.acuravigorclub.com/Timely-Topics/TTGraphics/0603/Screwdriver.jpg";
		public const string imgFancyChocolate = "http://yourdailychocolate.com/wp-content/uploads/2009/12/gourmet_chocolate_tart-5362.jpg";
		public const string imgFish = "http://3.bp.blogspot.com/-gRyjCHpjvMw/TdQLzxHXn3I/AAAAAAAAEkE/fC8mnV8HysE/s400/shark+attackfive.jpg";
	
	
	}
}

