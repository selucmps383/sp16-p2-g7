﻿using System;
using Xamarin.Forms;

namespace GroupSeven
{
	public class StyleLabel : Label
	{
		public StyleLabel ()
		{
			FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label));
			TextColor = Color.FromHex (Globals.Aquaish);
		}

		public StyleLabel (string text)
		{
			Text = text;
			FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label));
			TextColor = Color.FromHex (Globals.Aquaish);
		}
		public StyleLabel (string text, int font)
		{
			Text = text; 
			FontSize = font;
			TextColor = Color.FromHex (Globals.Aquaish);
		}
	}
}

