﻿using System;
using Xamarin.Forms;

namespace GroupSeven
{
	public class StyleEntryCell :EntryCell
	{
		public StyleEntryCell (string label, string placeholder, string text)
		{
			Label = label;
			Placeholder = placeholder;
			Text = text;
			LabelColor = Color.FromHex (Globals.Aquaish);

		}
	}
}

