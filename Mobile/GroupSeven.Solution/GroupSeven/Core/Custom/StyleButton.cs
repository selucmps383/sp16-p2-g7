﻿using System;
using Xamarin.Forms;

namespace GroupSeven
{
	public class StyleButton : Button
	{
		public StyleButton (string text)
		{
			Text = text;
			TextColor = Color.FromHex (Globals.Aquaish);
			FontSize = Device.GetNamedSize (NamedSize.Large, typeof(StyleButton));
			BackgroundColor = Color.FromHex(Globals.Blackish);
			HorizontalOptions = LayoutOptions.Center;
			BorderWidth = 2;
			BorderColor = Color.FromHex(Globals.Purplish);
		}
	}
}

