﻿using System;
using Xamarin.Forms;
namespace GroupSeven
{
	public class StyleImageCell : ImageCell
	{
		public StyleImageCell ()
		{
			
		}

		public StyleImageCell (string uri, string text)
		{
			ImageSource = ImageSource.FromUri (new Uri (uri));
			Text = text;
			TextColor = Color.FromHex(Globals.Aquaish);
		}
	}
}

