﻿using System;
using System.Collections.Generic;

namespace GroupSeven
{
	public class Product
	{
		public Product ()
		{
		}

		public int ID {	get;set;}
		public string Name {get;set;}
		public DateTime CreatedDate {get;set;}
		public DateTime LastModifiedDate {	get;set;}
		public decimal Price {	get;set;}
		public int InventoryCount {	get;set;}
		public string ImageUrl { get; set; }
		public Category Category {	get; set;}
		public Manufacturer Manufacturer {get;set;}
		public ICollection<Sale> Sales {get;set;}

	}
}

