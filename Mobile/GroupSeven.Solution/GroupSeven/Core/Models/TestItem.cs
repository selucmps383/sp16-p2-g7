﻿using System;

namespace GroupSeven
{
	public class TestItem
	{
		public TestItem ()
		{
		}
		public int Id {
			get;
			set;
		}
		public string Name {
			get;
			set;
		}
		public int Amount {
			get;
			set;
		}
	}
}

