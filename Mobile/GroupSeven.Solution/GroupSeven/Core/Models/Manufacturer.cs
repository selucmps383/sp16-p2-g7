﻿using System;
using System.Collections.Generic;

namespace GroupSeven
{
	public class Manufacturer
	{

		
		public int ID {	get; set; }
		public string Name { get; set; }
		public virtual ICollection<Product> Products { get; set; } = new List<Product>();

		public Manufacturer ()
		{
			
		}

		public Manufacturer (Manufacturer current)
		{
			ID = current.ID;
			Name = current.Name; 
			Products = current.Products;
		}
			
		
	}
}

