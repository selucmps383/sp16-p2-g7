﻿using System;

namespace GroupSeven
{
	public class User
	{
		public User ()
		{
		}
		public User (User loginUser)
		{
			Id = loginUser.Id;
			ApiKey = loginUser.ApiKey;
		}
		public int Id {
			get;
			set;
		}
		public string ApiKey {
			get;
			set;
		}
	}
}

