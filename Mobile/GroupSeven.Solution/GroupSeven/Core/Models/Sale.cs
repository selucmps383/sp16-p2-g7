﻿using System;
using System.Collections.Generic;

namespace GroupSeven
{
	public class Sale
	{
		public Sale ()
		{
		}
		public int ID {
			get;
			set;
		}
		public DateTime Date {
			get;
			set;
		}
		public decimal TotalAmount {
			get;
			set;
		}
		public string EmailAddress {
			get;
			set;
		}
	//	public ICollection<Product> Products {
	//		get;
	//		set;
	//	}


	}
}

