﻿using System;
using System.Collections.Generic;

namespace GroupSeven
{
	public class Category
	{

		public int ID {	get; set; }
		public string Name { get; set; }
		public virtual ICollection<Product> Products { get; set; } = new List<Product>();
		public Category ()
		{
			
		}
		public Category (Category current)
		{
			ID = current.ID;
			Name = current.Name; 
			Products = current.Products;
		}
	}
}

