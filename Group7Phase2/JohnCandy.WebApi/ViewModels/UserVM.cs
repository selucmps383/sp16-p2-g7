﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JohnCandy.WebApi.ViewModels
{
    public class UserVM
    {
        public int ID { get; set; }
        public string ApiKey { get; set; }
    }
}