﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JohnCandy.WebApi.Models;

namespace JohnCandy.WebApi.ViewModels
{
    public class SaleVM
    {
        public SaleVM(Sale toVM)
        {
            this.ID = toVM.ID;
            this.Date = toVM.Date;
            this.TotalAmount = toVM.TotalAmount;
            this.EmailAddress = toVM.EmailAddress;

            foreach (var product in toVM.Products)
            {
                while (product.Product.Manufacturer.Products != null)
                {
                    product.Product.Manufacturer.Products = null;
                }
                while (product.Product.Category.Products != null)
                {
                    product.Product.Category.Products = null;
                }
                while (product.Product.Sales != null)
                {
                    product.Product.Sales = null;
                }
                this.Products.Add(product);
            }
        }

        public int ID { get; set; }

        public DateTime Date { get; set; }

        public decimal TotalAmount { get; set; }

        public string EmailAddress { get; set; }

        public ICollection<ProductSaleDetails> Products { get; set; } = new List<ProductSaleDetails>();
    }
}