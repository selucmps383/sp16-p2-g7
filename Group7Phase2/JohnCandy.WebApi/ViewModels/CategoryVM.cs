﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JohnCandy.WebApi.Models;

namespace JohnCandy.WebApi.ViewModels
{
    public class CategoryVM
    {
        //So, here's what happened here... 
        //The code to set the product list to null
        //never executed the first time... the while loops
        //ensure that the product list is set to null.
        public CategoryVM(Category toVM)
        {
            this.ID = toVM.ID;
            this.Name = toVM.Name;
            foreach (var product in toVM.Products)
            {
                while (product.Sales != null)
                {
                    product.Sales = null;
                }
                while (product.Manufacturer.Products != null)
                {
                    product.Manufacturer.Products = null;
                }
                while (product.Category.Products != null)
                {
                    product.Category.Products = null;
                }
                this.Products.Add(product);
            }
        }

        public int ID { get; set; }

        public string Name { get; set; }

        public ICollection<Product> Products { get; set; } = new List<Product>();
    }
}