﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JohnCandy.WebApi.Models;

namespace JohnCandy.WebApi.ViewModels
{
    public class ManufacturerVM
    {
        public ManufacturerVM(Manufacturer toVM)
        {
            this.ID = toVM.ID;
            this.Name = toVM.Name;
            foreach (var product in toVM.Products)
            {
                while (product.Category.Products != null)
                {
                    product.Category.Products = null;
                }
                while (product.Manufacturer.Products != null)
                {
                    product.Manufacturer.Products = null;
                }
                while (product.Sales != null)
                {
                    product.Sales = null;
                }
                this.Products.Add(product);
            }
        }

        public int ID { get; set; }

        public string Name { get; set; }

        public ICollection<Product> Products { get; set; } = new List<Product>();
    }
}