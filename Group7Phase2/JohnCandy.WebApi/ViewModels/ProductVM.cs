﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JohnCandy.WebApi.Models;

namespace JohnCandy.WebApi.ViewModels
{
    public class ProductVM
    {
        public ProductVM(Product toVM)
        {
            this.ID = toVM.ID;
            this.Name = toVM.Name;
            this.CreatedDate = toVM.CreatedDate;
            this.LastModifiedDate = toVM.LastModifiedDate;
            this.Price = toVM.Price;
            this.InventoryCount = toVM.InventoryCount;
            this.Category = toVM.Category;
            this.Manufacturer = toVM.Manufacturer;
            this.ImageUrl = toVM.ImageUrl;

            while (this.Category.Products != null)
            {
                this.Category.Products = null;
            }
            while (this.Manufacturer.Products != null)
            {
                this.Manufacturer.Products = null;
            }
            while (this.Sales != null)
            {
                this.Sales = null;
            }
        }

        public int ID { get; set; }

        public string Name { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastModifiedDate { get; set; }

        public decimal Price { get; set; }

        public int InventoryCount { get; set; }

        public string ImageUrl { get; set; }

        public Category Category { get; set; }

        public Manufacturer Manufacturer { get; set; }

        public ICollection<Sale> Sales { get; set; } = new List<Sale>();
    }
}