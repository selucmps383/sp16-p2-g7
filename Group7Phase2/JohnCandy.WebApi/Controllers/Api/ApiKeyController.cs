﻿using System;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Http.Description;
using JohnCandy.WebApi.ViewModels;

namespace JohnCandy.WebApi.Controllers.Api
{
    public class ApiKeyController : ApiController
    {
        // GET: api/ApiKey?email=<email>,password=<password>
        [ResponseType(typeof(UserVM))]
        public async Task<IHttpActionResult> GetUser(string email, string password)
        {
            if (email == null || password == null)
            {
                return BadRequest();
            }
            using (var db = new WebContext())
            {
                var user = await db.Users.FirstAsync(u => u.Email == email);
                if (user == null || !Crypto.VerifyHashedPassword(user.Password, password))
                {
                    return ResponseMessage(new HttpResponseMessage(HttpStatusCode.Forbidden));
                }
                user.ApiKey = GetApiKey();
                db.Entry(user).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return Ok(new UserVM() { ID = user.ID, ApiKey = user.ApiKey });
            }
        }

        private static string GetApiKey()
        {
            using (var rng = new RNGCryptoServiceProvider())
            {
                var bytes = new byte[16];
                rng.GetBytes(bytes);
                return Convert.ToBase64String(bytes);
            }
        }
    }
}