﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using JohnCandy.WebApi;
using JohnCandy.WebApi.Models;
using JohnCandy.WebApi.ViewModels;
using JohnCandy.WebApi.Filters;

namespace JohnCandy.WebApi.Controllers.Api
{
    public class ManufacturersController : ApiController
    {
        private WebContext db = new WebContext();

        // GET: api/Manufacturers
        
        public ICollection<ManufacturerVM> GetManufacturers()
        {
            var retList = new List<ManufacturerVM>();
            foreach (var manufacturer in db.Manufacturers)
            {
                retList.Add(new ManufacturerVM(manufacturer));
            }
            return retList;
        }

        // GET: api/Manufacturers/5
        [ResponseType(typeof(ManufacturerVM))]
        public async Task<IHttpActionResult> GetManufacturer(int id)
        {
            Manufacturer manufacturer = await db.Manufacturers.FindAsync(id);
            if (manufacturer == null)
            {
                return NotFound();
            }

            return Ok(new ManufacturerVM(manufacturer));
        }

        // PUT: api/Manufacturers/5
        [ApiKeyValidation]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutManufacturer(int id, Manufacturer manufacturer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != manufacturer.ID)
            {
                return BadRequest();
            }

            db.Entry(manufacturer).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ManufacturerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Manufacturers
        [ApiKeyValidation]
        [ResponseType(typeof(ManufacturerVM))]
        public async Task<IHttpActionResult> PostManufacturer(Manufacturer manufacturer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Manufacturers.Add(manufacturer);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = manufacturer.ID }, new ManufacturerVM(manufacturer));
        }

        // DELETE: api/Manufacturers/5
        [ApiKeyValidation]
        [ResponseType(typeof(ManufacturerVM))]
        public async Task<IHttpActionResult> DeleteManufacturer(int id)
        {
            Manufacturer manufacturer = await db.Manufacturers.FindAsync(id);
            if (manufacturer == null)
            {
                return NotFound();
            }

            db.Manufacturers.Remove(manufacturer);
            await db.SaveChangesAsync();

            return Ok(new ManufacturerVM(manufacturer));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ManufacturerExists(int id)
        {
            return db.Manufacturers.Count(e => e.ID == id) > 0;
        }
    }
}