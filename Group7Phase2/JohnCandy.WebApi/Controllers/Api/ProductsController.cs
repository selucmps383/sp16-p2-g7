﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using JohnCandy.WebApi.Filters;
using JohnCandy.WebApi.Models;
using JohnCandy.WebApi.ViewModels;
using System;
using System.Data;

namespace JohnCandy.WebApi.Controllers.Api
{
    public class ProductsController : ApiController
    {
        private WebContext db = new WebContext();

        [HttpGet]
        public async Task<ICollection<ProductVM>> GetProducts()
        {
            var prods = await db.Products.ToListAsync();
            var prodVms = new List<ProductVM>();
            foreach (var product in prods)
            {
                prodVms.Add(new ProductVM(product));
            }
            return prodVms;
        }

        [HttpGet]
        [Route("api/Products/{id:int}")]
        public async Task<IHttpActionResult> GetProducts(int id)
        {
            var searchProd = await db.Products.FindAsync(id);
            if (searchProd == null) return NotFound();
            return Ok(new ProductVM(searchProd));
        }
        
        [ResponseType(typeof(ICollection<ProductVM>))]
        [Route("api/Products/Category/{categoryId:int}")]
        public async Task<IHttpActionResult> GetProductByCategory(int? categoryId)
        {
            if (categoryId == null)
            {
                return BadRequest("category Id is required");
            }
            var resultSet = new List<ProductVM>();
            foreach (var product in await db.Products.Where(p => p.Category.ID == categoryId).ToListAsync())
            {
                resultSet.Add(new ProductVM(product));
            }
            return Ok(resultSet);
        }

        // PUT: api/Products/5
        [Route("api/Products/{id:int}")]
        [ApiKeyValidation]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutProduct(int id, Product product)
        {
            Product originalProduct = await db.Products.FindAsync(id);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != product.ID)
            {
                return BadRequest();
            }

            product.Category = await db.Categories.FindAsync(product.Category.ID);
           
            if (product.Category == null)
            {
                return BadRequest();
            }
            
            product.Manufacturer = await db.Manufacturers.FindAsync(product.Manufacturer.ID);

            if (product.Manufacturer == null)
            {
                return BadRequest();
            }

            originalProduct = UpdateProduct(originalProduct, product);

            db.Entry(originalProduct).State = EntityState.Modified;
            
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    return BadRequest(ex.Message);
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Products
        [ApiKeyValidation]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PostProduct(Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var unique = await db.Products.FirstOrDefaultAsync(p => p.Name.ToLower() == product.Name.ToLower());
            if(unique != null)
            {
                return BadRequest();
            }

            product.Category = await db.Categories.FindAsync(product.Category.ID);
            if (product.Category == null)
            {
                return BadRequest();

            }

            product.Manufacturer = await db.Manufacturers.FindAsync(product.Manufacturer.ID);

            db.Products.Add(product);

            await db.SaveChangesAsync();
                 

            return Ok();// CreatedAtRoute("DefaultApi", new { id = product.ID }, new ProductVM(product));
        }

        // DELETE: api/Products/5
        [ApiKeyValidation]
        [Route("api/Products/{id:int}")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> DeleteProduct(int id)
        {
            Product product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            db.Products.Remove(product);


            await db.SaveChangesAsync();

            return Ok(); //(new ProductVM(product));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductExists(int id)
        {
            return db.Products.Count(e => e.ID == id) > 0;
        }

        private Product UpdateProduct(Product originalProduct, Product editedProduct) {
            originalProduct.ID = editedProduct.ID;
            originalProduct.Name = editedProduct.Name;
            originalProduct.Price = editedProduct.Price;
            originalProduct.InventoryCount = editedProduct.InventoryCount;
            originalProduct.LastModifiedDate = editedProduct.LastModifiedDate;
            originalProduct.ImageUrl = editedProduct.ImageUrl;
            originalProduct.Category = editedProduct.Category;
            originalProduct.Manufacturer = editedProduct.Manufacturer;

            return originalProduct;

        }
    }
}
