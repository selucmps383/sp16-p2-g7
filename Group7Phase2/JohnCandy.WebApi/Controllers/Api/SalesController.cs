﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using JohnCandy.WebApi;
using JohnCandy.WebApi.Models;
using JohnCandy.WebApi.ViewModels;
using Microsoft.Ajax.Utilities;

namespace JohnCandy.WebApi.Controllers.Api
{
    public class SalesController : ApiController
    {
        private WebContext db = new WebContext();

        // GET: api/Sales
        [HttpGet]
        public ICollection<SaleVM> GetSales()
        {
            var retList = new List<SaleVM>();
            foreach (var sale in db.Sales)
            {
                retList.Add(new SaleVM(sale));
            }
            return retList;
        }

        // GET: api/Sales/5
        [ResponseType(typeof(SaleVM))]
        public async Task<IHttpActionResult> GetSale(int id)
        {
            Sale sale = await db.Sales.FindAsync(id);
            if (sale == null)
            {
                return NotFound();
            }

            return Ok(new SaleVM(sale));
        }

        // PUT: api/Sales/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSale(int id, Sale sale)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sale.ID)
            {
                return BadRequest();
            }

            db.Entry(sale).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SaleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Sales
        [HttpPost]
        [Route("api/Sales", Name = "PostSale")]
        [ResponseType(typeof(SaleVM))]
        public async Task<IHttpActionResult> PostSale(Sale sale)
        {
            if (sale?.Products == null || sale.EmailAddress == null)
            {
                return BadRequest(ModelState);
            }
            sale.Date = DateTime.UtcNow;
            var finalProds = new List<ProductSaleDetails>();

            foreach (var saleDetail in sale.Products)
            {
                var dbProduct = await db.Products.FindAsync(saleDetail.Product.ID);
                if (dbProduct.InventoryCount < saleDetail.Quantity)
                    return
                        BadRequest("Cannot purchase " + saleDetail.Quantity + " of " + dbProduct.Name + ". " +
                                   dbProduct.Name + " has an inventory count of " +
                                   dbProduct.InventoryCount);
                dbProduct.Category = dbProduct.Category;
                dbProduct.InventoryCount -= saleDetail.Quantity;
                db.Entry(dbProduct).State = EntityState.Modified;
                finalProds.Add(new ProductSaleDetails() { Product = dbProduct, Quantity = saleDetail.Quantity });
            }

            sale.Products = finalProds;
            sale.TotalAmount = CalculateSale(sale);
            db.Sales.Add(sale);

            await db.SaveChangesAsync();

            SendEmail(sale);

            return CreatedAtRoute("PostSale", new { id = sale.ID }, new SaleVM(sale));
        }

        private void SendEmail(Sale purchasedCart)
        {
            using (var client = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587))
            {
                client.Credentials = new System.Net.NetworkCredential("test.webapi01@gmail.com", "cmps383phase2");
                client.EnableSsl = true;
                var message = new System.Net.Mail.MailMessage("test.webapi01@gmail.com", purchasedCart.EmailAddress,
                    "Purchase on the John Candy web store",
                    "Your recent purchase:\n\tTotal: " + purchasedCart.TotalAmount.ToString("C") +
                    "\n\tItems:\n\t\t" +
                    String.Join("\n\t\t", purchasedCart.Products.Select(p => p.Product.Name + " (x" + p.Quantity + ") - " + p.Product.Price.ToString("C"))));
                client.Send(message);
            }
        }

        [HttpPut]
        [Route("api/Sale/Calculate/")]
        public async Task<IHttpActionResult> CalculateTotal(Sale sale)
        {
            if (sale == null) return BadRequest();
            var calculate = new List<ProductSaleDetails>();
            foreach (var saleProductDetail in sale.Products)
            {
                saleProductDetail.Product = await db.Products.FindAsync(saleProductDetail.Product.ID);
                if (saleProductDetail.Product == null) return NotFound();
                calculate.Add(saleProductDetail);
            }
            return Ok(CalculateProducts(calculate));
        }

        public decimal CalculateSale(Sale sale)
        {
            return CalculateProducts(sale.Products);
        }

        public decimal CalculateProducts(IEnumerable<ProductSaleDetails> products)
        {
            var total = 0M;
            Parallel.ForEach(products, pr => total += (pr.Product.Price * pr.Quantity));
            return total;
        }

        // DELETE: api/Sales/5
        [ResponseType(typeof(SaleVM))]
        public async Task<IHttpActionResult> DeleteSale(int id)
        {
            Sale sale = await db.Sales.FindAsync(id);
            if (sale == null)
            {
                return NotFound();
            }

            db.Sales.Remove(sale);
            await db.SaveChangesAsync();

            return Ok(new SaleVM(sale));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SaleExists(int id)
        {
            return db.Sales.Count(e => e.ID == id) > 0;
        }
    }
}