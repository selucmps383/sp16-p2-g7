﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using JohnCandy.WebApi.Filters;
using JohnCandy.WebApi.Models;
using JohnCandy.WebApi.ViewModels;

namespace JohnCandy.WebApi.Controllers.Api
{
    public class CategoriesController : ApiController
    {
        private WebContext db = new WebContext();
        
        // GET: api/Categories
        public ICollection<CategoryVM> GetCategories()
        {
            var retList = new List<CategoryVM>();
            foreach (var category in db.Categories)
            {
                retList.Add(new CategoryVM(category));
            }
            return retList;
        }

        // GET: api/Categories/5
        [ResponseType(typeof(CategoryVM))]
        public async Task<IHttpActionResult> GetCategory(int id)
        {
            var dbCategory = await db.Categories.FirstOrDefaultAsync(c => c.ID == id);
            if (dbCategory == null)
            {
                return NotFound();
            }
            var category = new CategoryVM(dbCategory);
            return Ok(category);
        }

        // PUT: api/Categories/5
        [ApiKeyValidation]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCategory(int id, Category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != category.ID)
            {
                return BadRequest();
            }

            db.Entry(category).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Categories
        [ApiKeyValidation]
        [ResponseType(typeof(CategoryVM))]
        public async Task<IHttpActionResult> PostCategory(Category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Categories.Add(category);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = category.ID }, new CategoryVM(category));
        }

        // DELETE: api/Categories/5
        [ApiKeyValidation]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> DeleteCategory(int id)
        {
            Category category = await db.Categories.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            db.Categories.Remove(category);
            await db.SaveChangesAsync();

            return Ok();//(new CategoryVM(category));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategoryExists(int id)
        {
            return db.Categories.Count(e => e.ID == id) > 0;
        }
    }
}