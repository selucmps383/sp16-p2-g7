﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JohnCandy.WebApi.Models
{
    public class ProductSaleDetails
    {
        [Key]
        public int ID { get; set; }
        public int Quantity { get; set; }

        public Product Product { get; set; }
    }
}