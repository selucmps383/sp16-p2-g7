﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace JohnCandy.WebApi.Models
{
    public class Category
    {
        [Key]
        public int ID { get; set; }

        [Column(TypeName = "varchar")]
        [StringLength(512, ErrorMessage = "Name can be no more than 512 characters")]
        public string Name { get; set; }

        public virtual ICollection<Product> Products { get; set; } = new List<Product>();
    }
}