﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace JohnCandy.WebApi.Models
{
    public class Product
    {
        [Key]
        public int ID { get; set; }

        [Index(IsUnique = true)]
        [Column(TypeName = "varchar")]
        [StringLength(512, ErrorMessage = "Name can be no more than 512 characters")]
        public string Name { get; set; }

        [DisplayName("Created Date")]
        [Column(TypeName = "date")]
        [DataType(DataType.Date)]
        public DateTime CreatedDate { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayName("Last Modified Date")]
        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDate { get; set; }

        [Range(0.0, 9999999999999999.99)]
        public decimal Price { get; set; }

        [Range(0, Int32.MaxValue)]
        [DisplayName("Inventory Count")]
        public int InventoryCount { get; set; }

        [Column(TypeName = "varchar")]
        [StringLength(512, ErrorMessage = "Image Url can be no more than 512 characters")]
        public string ImageUrl { get; set; }

        [Required]
        public virtual Category Category { get; set; }

        public virtual Manufacturer Manufacturer { get; set; }
        
        public virtual ICollection<Sale> Sales { get; set; } = new List<Sale>();
    }
}