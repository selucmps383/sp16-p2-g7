﻿(function () {
    "use strict";

    //Getting the existing Module
    angular.module("main")
        //Adding the new controller to the module
        .controller("home", homeController);

    function homeController($scope) {
        $scope.products = [
        {
            id: 10,
            name: "Wrench",
            price: "1.99",
            createdDate: "",
            lastModifiedDate: "",
            inventoryCount: "5",
            category: "Tools",
            manufacturer: "Kobalt",
        },
        {
            id: 11,
            name: "Hammer",
            price: "1.99",
            createdDate: "",
            lastModifiedDate: "",
            inventoryCount: "5",
            category: "Tools",
            manufacturer: "Lowes",
        },
            {
                id: 115,
                name: "Hammer",
                price: "1.99",
                createdDate: "",
                lastModifiedDate: "",
                inventoryCount: "5",
                category: "Tools",
                manufacturer: "Lowes",
            },
                {
                    id: 101,
                    name: "Hammer",
                    price: "1.99",
                    createdDate: "",
                    lastModifiedDate: "",
                    inventoryCount: "5",
                    category: "Tools",
                    manufacturer: "Lowes",
                },

             {
                 id: 133,
                 name: "Wrench",
                 price: "1.99",
                 createdDate: "",
                 lastModifiedDate: "",
                 inventoryCount: "5",
                 category: "Tools",
                 manufacturer: "Kobalt",
        }];
    }
})();