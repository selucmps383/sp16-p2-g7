﻿(function () {
    "use strict";
    var main = angular.module("main", ['ngRoute'])
            .config(['$routeProvider', function ($routeProvider) {
                $routeProvider
                $routeProvider.when('/', {
                    templateUrl: '/Shop/productList.html',
                    controller: "productsListController"
                })
                $routeProvider.when('/detail/:productSku', {
                    templateUrl: '/Shop/productDetails.html',
                    controller: 'productDetailsController'
                })
                $routeProvider.when('/cart', {
                    templateUrl: '/Shop/checkout.html',
                    controller: "cartController",
                })
                $routeProvider.otherwise({
                    redirectTo: '/'
                });
            }]);
})();


angular.module('main')
.factory('dataFactory', ['$http', function ($http) {

    var urlBase = 'http://localhost:10194//api/products';
    var urlBase1 = 'http://localhost:10194//api/categories';
    var dataFactory = {};

    dataFactory.getProducts = function () {
        return $http.get(urlBase);
    };

    dataFactory.getCategories = function () {
        return $http.get(urlBase);
    };
    dataFactory.putProducts = function () {
        return $http.get(urlBase);
    };

    return dataFactory;
}]);

angular.module('main')
    .controller('apiController', ['$scope', 'dataFactory',
        function ($scope, dataFactory) {

            $scope.products;

            getProducts();

            function getProducts() {
                dataFactory.getProducts()
                .success(function (products) {
                    $scope.products = products;
                    console.log(products);
                })
                .error(function (error) {
                    $scope.status = 'Unable to load product ' + error.message;
                });
            };
        }]);




angular.module('main')
.factory('dataFactory', ['$http', function ($http) {

    var urlBase = 'http://localhost:10194//api/products';
    var urlBase1 = 'http://localhost:10194//api/categories';
    var dataFactory = {};

    dataFactory.getProducts = function () {
        return $http.get(urlBase);
    };

    dataFactory.getCategories = function () {
        return $http.get(urlBase);
    };
    dataFactory.putProducts = function () {
        return $http.get(urlBase);
    };

    return dataFactory;
}]);

angular.module('mainApp')
    .controller('apiController', ['$scope', 'dataFactory',
        function ($scope, dataFactory) {

            $scope.products;

            getProducts();

            function getProducts() {
                dataFactory.getProducts()
                .success(function (products) {
                    $scope.products = products;
                    console.log(products);
                })
                .error(function (error) {
                    $scope.status = 'Unable to load product ' + error.message;
                });
            };
        }]);


//just a test
angular.module('mainApp')
    .controller('mka', mka);

function mka($http) {

    var vm = this;
    vm.products = [];
    vm.newProduct = {};
    vm.errorMessage = "";

    $http.get('/api/products')
        .then(function (response) {

            angular.copy(response.data, vm.products);
        }, function (error) {
            vm.errorMessage = "Failed to laod data: " + error;
        });
}

angular.module('mainApp')
    .controller('getCategories', ['$scope', 'dataFactory',
        function ($scope, dataFactory) {

            $scope.categories;

            getCategories();

            function getCategories() {
                dataFactory.getCategories()
                .success(function (category) {
                    $scope.categories = category;
                    console.log(category)
                })
                .error(function (error) {
                    $scope.status = 'Unable to load product ' + error.message;
                });
            };
        }]);