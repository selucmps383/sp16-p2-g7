﻿(function () {
    'use strict';

    angular
		.module('mainApp')
		.controller('CartCtrl', ['itemService', 'alertService','$window', function (itemService, alertService, $window) {

		    
		    var cartCtrl = this;

		    cartCtrl.clearCart = itemService.clearCart;
		    cartCtrl.alerts = alertService.get();
		    cartCtrl.getTotal = itemService.getTotal;
		    cartCtrl.click = itemService.click;
		    cartCtrl.addSale = itemService.addSale;
		    cartCtrl.removeItem = itemService.removeItem;
		    cartCtrl.increment = itemService.increment;
		    cartCtrl.decrement = itemService.decrement;
		    cartCtrl.items = itemService.cart;



		    cartCtrl.getTotal = function () {
		        var total = 0;
		        angular.forEach(cartCtrl.items, function (item) {
		            total += item.Quantity * item.Price;
		        })
		        return total;
		    }
            
		    cartCtrl.getQty = function () {
		        return value = 0;
		        console.log(cartCtrl.items.Quantity);
		    }


		    cartCtrl.getNumber = function () {
		        return cartCtrl.items.length;
		        console.log(cartCtrl.items.length);
		    }

		    cartCtrl.getProducts = function () {
		        return cartCtrl.items;
		    }

		  

             

		}]);
            
})();