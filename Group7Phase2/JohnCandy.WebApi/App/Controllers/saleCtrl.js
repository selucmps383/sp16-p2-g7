﻿(function () {
    'use strict';

    angular
		.module('mainApp')
		.controller('SaleCtrl', SaleCtrl);



    SaleCtrl.$inject = ['$http', 'itemService'];

    function SaleCtrl($http, itemService) {
        var saleCtrl = this;

        saleCtrl.currentItem = itemService.current;
        saleCtrl.addToCart = itemService.addToCart;
        saleCtrl.selectItem = itemService.selectItem;
        saleCtrl.clearSelection = itemService.clearSelection;
        saleCtrl.removeItem = itemService.removeItem;
        saleCtrl.click = itemService.click;
        saleCtrl.addSale = itemService.addSale;

        $http
            .get('http://localhost:10194/api/Sales')
            .then(function (response) {
                saleCtrl.items = response.data;
                console.log(angular.toJson(saleCtrl.items) + "Salectrl items")
            });

    };

})();