﻿(function () {
    'use strict';

    angular
		.module('mainApp')
		.controller('ListCtrl', ListCtrl);

    

    ListCtrl.$inject = ['$http', 'itemService'];

    function ListCtrl($http, itemService) {

        var listCtrl = this;

   

        listCtrl.sortStuff = itemService.sortStuff;
        listCtrl.currentItem = itemService.current;
        listCtrl.addToCart = itemService.addToCart;
        listCtrl.selectItem = itemService.selectItem;
        listCtrl.clearSelection = itemService.clearSelection;
        listCtrl.removeItem = itemService.removeItem;
        listCtrl.click = itemService.click;
        listCtrl.addSale = itemService.addSale;

        $http
        .get('http://localhost:10194/api/Products')
        .then(function (response) {
            listCtrl.items = response.data;
        }, function (response) {
            listCtrl.items = "Unable to load products. Please call someone who cares."

        });


        //$http
        //    .get('http://localhost:10194/api/Categories')
        //    .then(function (response) {
        //        listCtrl.categories = response.data;
        //        console.log(listCtrl.categories + "Listctrl categories")
        //    })
        //$http
        //    .get('http://localhost:10194/api/Manufacturers')
        //    .then(function (response) {
        //        listCtrl.manufacturers = response.data;
        //        console.log(listCtrl.manufacturers + "Listctrl manufacturers")
        //    })


    }


        
 
})();