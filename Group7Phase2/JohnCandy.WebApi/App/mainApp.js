﻿// gather all controllers into one specfic space
// definded by name of the model(mainApp) and list of dependices(listController)

(function() {
    var mainApp = angular.module('mainApp', [/*'ui.router',*/ 'ngRoute', 'angular.filter'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider
        $routeProvider.when('/', {
            templateUrl: '/App/Views/productList.html',
            controller: 'ListCtrl',
        })
        $routeProvider.when('/detail/:id', {
            templateUrl: '/App/Views/productDetails.html',
           // controller: 'productDetailsController'
        })
        $routeProvider.when('/cart', {
            templateUrl: '/App/Views/cart.html',
            controller: "CartCtrl",
            controller: 'ListCtrl'
        })
        $routeProvider.when('/admin', {
            templateUrl: '/App/Views/admin.html',
            controller: 'ListCtrl',
            controller: 'CartCtrl'
        })
        $routeProvider.otherwise({
            redirectTo: '/'
        });
    }])



      .directive('selectpicker', ['$parse', function ($parse) {
          return {
              restrict: 'A',
              priority: 10,
              compile: function (tElement, tAttrs, transclude) {
                  tElement.selectpicker($parse(tAttrs.selectpicker)());
                  tElement.selectpicker('refresh');
                  return function (scope, element, attrs, ngModel) {
                      if (!ngModel) return;

                      scope.$watch(attrs.ngModel, function (newVal, oldVal) {
                          scope.$evalAsync(function () {
                              if (!attrs.ngOptions || /track by/.test(attrs.ngOptions)) element.val(newVal);
                              element.selectpicker('refresh');
                          });
                      });

                      ngModel.$render = function () {
                          scope.$evalAsync(function () {
                              element.selectpicker('refresh');
                          });
                      }
                  };
              }

          };
      }]);



})();