﻿(function() {
    'use strict';

    angular
		.module('mainApp')
		.factory('itemService', itemService);

    itemService.$inject = ['$http', '$window'];

    function itemService($http) {
        var service = {
            current: {},
            filters: {},
            cart: [],
            sale: [],
            addSale: addSale,
            selectItem: selectItem,
            clearSelection: clearSelection,
            addToCart: addToCart,
            removeItem: removeItem,
            click: click,
            clearCart: clearCart,

        };

       

        function addSale(items, email) {

            
            console.log(items);
            var saleItems = [];

            for (var i = 0; i < items.length; i++) {
                saleItems.push({ Quantity: items[i].Quantity, Product: items[i] })
                
            }
            console.log(saleItems);
          

            var sale = {

                
                EmailAddress: email,
                Products: saleItems,
                
                
            };

            console.log(angular.toJson(items) + "before http items");
            console.log(angular.toJson(sale) + "before post");
            
            $http
                .post('http://localhost:10194/api/Sales', sale) 
                .then(function (response) {
                    console.log(angular.toJson(sale) + "after");
                    clearCart();
                    window.location.href = '#/';
                   
                }, function (response) {
                    alert("Something went wrong - please resend confirmation");
                })

        
    }

        function selectItem(item) {

            $http
				.get('http://localhost:10194/api/Products') 
				.then(function (response) {
				    angular.copy(item, service.current);
				    service.current.item = response.data[0];
				});	
        }

        function clearSelection() {
            angular.copy({}, service.current);
        }

        function addToCart(item, Quantity) {

  
            item.Quantity = Quantity;
            item.Quantity = 1;
            var copy = angular.copy(item);
            service.cart.push(copy);
            console.log(copy)
            


        }
        itemService.inCart = function (Quantity) {
            return addToCart(Quantity)
            console.log(item.Quantity + "works?")
        }


        function filterCat(item) {
            if (item.Category.Name == item.Category.Name)
                return item;
        }


        function removeItem(index) {
            console.log("remove item");
            console.log(index)
            service.cart.splice(index, 1);

        }

        function clearCart() {
            service.cart.length = 0;
            
        }

        function click(item) {
            
            
            //return itemService.cart.indexOf(item) !== -1;
            //console.log(item + "click");

        }


        return service;
    }

})();