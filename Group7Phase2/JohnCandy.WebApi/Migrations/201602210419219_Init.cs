namespace JohnCandy.WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 512, unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 512, unicode: false),
                        CreatedDate = c.DateTime(nullable: false, storeType: "date"),
                        LastModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InventoryCount = c.Int(nullable: false),
                        Category_ID = c.Int(nullable: false),
                        Manufacturer_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Categories", t => t.Category_ID, cascadeDelete: true)
                .ForeignKey("dbo.Manufacturers", t => t.Manufacturer_ID)
                .Index(t => t.Name, unique: true)
                .Index(t => t.Category_ID)
                .Index(t => t.Manufacturer_ID);
            
            CreateTable(
                "dbo.Manufacturers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 512, unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Sales",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false, storeType: "date"),
                        TotalAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EmailAddress = c.String(maxLength: 512, unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Email = c.String(maxLength: 512, unicode: false),
                        FName = c.String(maxLength: 512, unicode: false),
                        LName = c.String(maxLength: 512, unicode: false),
                        Password = c.String(maxLength: 128, unicode: false),
                        ApiKey = c.String(maxLength: 512, unicode: false),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.Email, unique: true);
            
            CreateTable(
                "dbo.SaleProducts",
                c => new
                    {
                        Sale_ID = c.Int(nullable: false),
                        Product_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Sale_ID, t.Product_ID })
                .ForeignKey("dbo.Sales", t => t.Sale_ID, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.Product_ID, cascadeDelete: true)
                .Index(t => t.Sale_ID)
                .Index(t => t.Product_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SaleProducts", "Product_ID", "dbo.Products");
            DropForeignKey("dbo.SaleProducts", "Sale_ID", "dbo.Sales");
            DropForeignKey("dbo.Products", "Manufacturer_ID", "dbo.Manufacturers");
            DropForeignKey("dbo.Products", "Category_ID", "dbo.Categories");
            DropIndex("dbo.SaleProducts", new[] { "Product_ID" });
            DropIndex("dbo.SaleProducts", new[] { "Sale_ID" });
            DropIndex("dbo.Users", new[] { "Email" });
            DropIndex("dbo.Products", new[] { "Manufacturer_ID" });
            DropIndex("dbo.Products", new[] { "Category_ID" });
            DropIndex("dbo.Products", new[] { "Name" });
            DropTable("dbo.SaleProducts");
            DropTable("dbo.Users");
            DropTable("dbo.Sales");
            DropTable("dbo.Manufacturers");
            DropTable("dbo.Products");
            DropTable("dbo.Categories");
        }
    }
}
