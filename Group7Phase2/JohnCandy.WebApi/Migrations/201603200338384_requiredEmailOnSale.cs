namespace JohnCandy.WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class requiredEmailOnSale : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SaleProducts", "Sale_ID", "dbo.Sales");
            DropForeignKey("dbo.SaleProducts", "Product_ID", "dbo.Products");
            DropIndex("dbo.SaleProducts", new[] { "Sale_ID" });
            DropIndex("dbo.SaleProducts", new[] { "Product_ID" });
            CreateTable(
                "dbo.ProductSaleDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        Product_ID = c.Int(),
                        Sale_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Products", t => t.Product_ID)
                .ForeignKey("dbo.Sales", t => t.Sale_ID)
                .Index(t => t.Product_ID)
                .Index(t => t.Sale_ID);
            
            AddColumn("dbo.Sales", "Product_ID", c => c.Int());
            AlterColumn("dbo.Sales", "EmailAddress", c => c.String(nullable: false, maxLength: 512, unicode: false));
            CreateIndex("dbo.Sales", "Product_ID");
            AddForeignKey("dbo.Sales", "Product_ID", "dbo.Products", "ID");
            DropTable("dbo.SaleProducts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SaleProducts",
                c => new
                    {
                        Sale_ID = c.Int(nullable: false),
                        Product_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Sale_ID, t.Product_ID });
            
            DropForeignKey("dbo.Sales", "Product_ID", "dbo.Products");
            DropForeignKey("dbo.ProductSaleDetails", "Sale_ID", "dbo.Sales");
            DropForeignKey("dbo.ProductSaleDetails", "Product_ID", "dbo.Products");
            DropIndex("dbo.ProductSaleDetails", new[] { "Sale_ID" });
            DropIndex("dbo.ProductSaleDetails", new[] { "Product_ID" });
            DropIndex("dbo.Sales", new[] { "Product_ID" });
            AlterColumn("dbo.Sales", "EmailAddress", c => c.String(maxLength: 512, unicode: false));
            DropColumn("dbo.Sales", "Product_ID");
            DropTable("dbo.ProductSaleDetails");
            CreateIndex("dbo.SaleProducts", "Product_ID");
            CreateIndex("dbo.SaleProducts", "Sale_ID");
            AddForeignKey("dbo.SaleProducts", "Product_ID", "dbo.Products", "ID", cascadeDelete: true);
            AddForeignKey("dbo.SaleProducts", "Sale_ID", "dbo.Sales", "ID", cascadeDelete: true);
        }
    }
}
