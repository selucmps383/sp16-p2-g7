using System.Web.Helpers;
using JohnCandy.WebApi.Models;

namespace JohnCandy.WebApi.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<JohnCandy.WebApi.WebContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(JohnCandy.WebApi.WebContext context)
        {
            context.Users.AddOrUpdate(u => u.Email,
            new User()
            {
                Email = "sa@383.com",
                Password = Crypto.HashPassword("password"),
                FName = "Admin",
                LName = "383"
            });

            var HomeDepot = new Manufacturer() { Name = "Home Depot", ID = 1 };
            var Walmart = new Manufacturer() { Name = "Walmart", ID = 2 };
            var Ghirardelli = new Manufacturer() { Name = "Ghirardelli", ID = 3 };
            
            context.Manufacturers.AddOrUpdate(u => u.ID, HomeDepot, Walmart, Ghirardelli);

            var Food = new Category() { Name = "Food", ID = 1 };
            var Tools = new Category() { Name = "Tools", ID = 2 };
            var Animals = new Category() { Name = "Animals", ID = 3 };
            context.Categories.AddOrUpdate(u => u.ID, Food, Tools, Animals);

            context.Products.AddOrUpdate(u => u.Name,
                new Product()
                {
                    Category = Food,
                    CreatedDate = DateTime.UtcNow,
                    InventoryCount = 15,
                    LastModifiedDate = DateTime.UtcNow,
                    Manufacturer = Ghirardelli,
                    Name = "Fancy Chocolate",
                    Price = 19.99M, 
                    ImageUrl = "http://yourdailychocolate.com/wp-content/uploads/2009/12/gourmet_chocolate_tart-5362.jpg"
                },
                new Product()
                {
                    Category = Food,
                    CreatedDate = DateTime.UtcNow,
                    InventoryCount = 75,
                    LastModifiedDate = DateTime.UtcNow,
                    Manufacturer = Walmart,
                    Name = "Cheap Chocolate",
                    Price = 5.99M,
                    ImageUrl = "https://guiadecontactomedico.files.wordpress.com/2011/09/chocolate-amrgo.jpg"
                },
                new Product()
                {
                    Category = Food,
                    CreatedDate = DateTime.UtcNow,
                    InventoryCount = 95,
                    LastModifiedDate = DateTime.UtcNow,
                    Manufacturer = Walmart,
                    Name = "Gummy Bears",
                    Price = 3.99M,
                    ImageUrl = "http://partystock.ca/media/catalog/product/cache/1/small_image/178x/9df78eab33525d08d6e5fb8d27136e95/g/u/gummi_bears.jpg"
                },
                new Product()
                {
                    Category = Food,
                    CreatedDate = DateTime.UtcNow,
                    InventoryCount = 75,
                    LastModifiedDate = DateTime.UtcNow,
                    Manufacturer = Walmart,
                    Name = "Lollipop",
                    Price = 3.99M,
                    ImageUrl = "http://www.29thstreetdesigns.com/item_images/Bir4_PinkLollipop_Thumbsmall.jpg"
                },
            
                new Product()
                {
                    Category = Food,
                    CreatedDate = DateTime.UtcNow,
                    InventoryCount = 15,
                    LastModifiedDate = DateTime.UtcNow,
                    Manufacturer = Walmart,
                    Name = "Snickers",
                    Price = 1.99M,
                    ImageUrl = "http://wildo.ru/wp-content/uploads/2011/03/Snickers.jpg"
                },
                
                new Product()
                {
                    Category = Tools,
                    CreatedDate = DateTime.UtcNow,
                    InventoryCount = 5,
                    LastModifiedDate = DateTime.UtcNow,
                    Manufacturer = HomeDepot,
                    Name = "Wrench",
                    Price = 3.99M,
                    ImageUrl = "http://www.supplyairtools.com/pro_small/wrench/monkey_wrench.jpg"
                },
                
                new Product()
                {
                    Category = Tools,
                    CreatedDate = DateTime.UtcNow,
                    InventoryCount = 5,
                    LastModifiedDate = DateTime.UtcNow,
                    Manufacturer = Walmart,
                    Name = "Hammer",
                    Price = 3.99M,
                    ImageUrl = "https://archadeckofcentralsouthcarolina.files.wordpress.com/2015/01/golden-hammer.jpg"
                },
                new Product()
                {
                    Category = Tools,
                    CreatedDate = DateTime.UtcNow,
                    InventoryCount = 25,
                    LastModifiedDate = DateTime.UtcNow,
                    Manufacturer = Ghirardelli,
                    Name = "Chocolate Hammer",
                    Price = 6.99M,
                    ImageUrl = "http://www.burstschocolates.com/var/images/product/300.300/459.jpg"
                },
                new Product()
                {
                    Category = Tools,
                    CreatedDate = DateTime.UtcNow,
                    InventoryCount = 7,
                    LastModifiedDate = DateTime.UtcNow,
                    Manufacturer = HomeDepot,
                    Name = "Shovel",
                    Price = 9.99M,
                    ImageUrl = "http://images-en.busytrade.com/154128400/Sell-Shovel-spade.jpg"
                },
                new Product()
                {
                    Category = Tools,
                    CreatedDate = DateTime.UtcNow,
                    InventoryCount = 95,
                    LastModifiedDate = DateTime.UtcNow,
                    Manufacturer = HomeDepot,
                    Name = "Screwdriver",
                    Price = 0.99M,
                    ImageUrl = "http://www.acuravigorclub.com/Timely-Topics/TTGraphics/0603/Screwdriver.jpg"
                },
               
                new Product()
                {
                    Category = Animals,
                    CreatedDate = DateTime.UtcNow,
                    InventoryCount = 25,
                    LastModifiedDate = DateTime.UtcNow,
                    Manufacturer = Ghirardelli,
                    Name = "Fish",
                    Price = 39.99M,
                    ImageUrl = "http://3.bp.blogspot.com/-gRyjCHpjvMw/TdQLzxHXn3I/AAAAAAAAEkE/fC8mnV8HysE/s400/shark+attackfive.jpg"
                },
                new Product()
                {
                    Category = Animals,
                    CreatedDate = DateTime.UtcNow,
                    InventoryCount = 3,
                    LastModifiedDate = DateTime.UtcNow,
                    Manufacturer = Walmart,
                    Name = "Chameleon",
                    Price = 125.99M,
                    ImageUrl = "http://thumb101.shutterstock.com/photos/thumb_large/349087/171899567.jpg"
                },
                  new Product()
                  {
                      Category = Food,
                      CreatedDate = DateTime.UtcNow,
                      InventoryCount = 75,
                      LastModifiedDate = DateTime.UtcNow,
                      Manufacturer = Walmart,
                      Name = "Cereal",
                      Price = 5.99M,
                      ImageUrl = "http://www.extension.uidaho.edu/diabetesplate/food/img/starch_cereal.jpg"
                  },
                    new Product()
                    {
                        Category = Food,
                        CreatedDate = DateTime.UtcNow,
                        InventoryCount = 75,
                        LastModifiedDate = DateTime.UtcNow,
                        Manufacturer = Walmart,
                        Name = "Ice Cream",
                        Price = 5.99M,
                        ImageUrl = "http://pensieve.typepad.com/pensieve/images/2007/08/12/chocolate_ice_cream.jpg"
                    },
                      new Product()
                      {
                          Category = Animals,
                          CreatedDate = DateTime.UtcNow,
                          InventoryCount = 75,
                          LastModifiedDate = DateTime.UtcNow,
                          Manufacturer = Walmart,
                          Name = "Pikachu",
                          Price = 5.99M,
                          ImageUrl = "https://upload.wikimedia.org/wikipedia/uk/a/a2/025-Pikachu.jpg"
                      },
                new Product()
                {
                    Category = Food,
                    CreatedDate = DateTime.UtcNow,
                    InventoryCount = 25,
                    LastModifiedDate = DateTime.UtcNow,
                    Manufacturer = Walmart,
                    Name = "Ice",
                    Price = 25.99M,
                    ImageUrl = "http://processspecialist.com/increasesales/wp-content/uploads/2012/06/Ice-MS-00.jpg"
                    });
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
