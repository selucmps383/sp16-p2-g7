namespace JohnCandy.WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class requiredEmailOnSale : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Sales", "EmailAddress", c => c.String(nullable: false, maxLength: 512, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Sales", "EmailAddress", c => c.String(maxLength: 512, unicode: false));
        }
    }
}
