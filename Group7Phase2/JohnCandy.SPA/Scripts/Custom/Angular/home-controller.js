﻿(function () {
    "use strict";

    //Getting the existing Module
    angular.module("main")
        //Adding the new controller to the module
        .controller("home", homeController);

    function homeController($scope) {
        $scope.products = [
        {
            name: "Candy Corn"
        },
        {
            name: "Ice Cream"
        },
        {
            name: "Popsicle"
        }];
    }
})();